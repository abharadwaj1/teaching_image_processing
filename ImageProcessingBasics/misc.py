# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 16:23:05 2020

@author: abharadwaj1
"""
import numpy as np
from numpy import sin,pi
import matplotlib.pyplot as plt
import matplotlib.image as image
from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider, IntSlider
import ipywidgets as widgets
from scipy.interpolate import interp1d
import math
from skimage.color import rgb2gray
import pandas as pd
import cmath
import matplotlib.patches as patches
from colorsys import hsv_to_rgb
from math import sin,cos,sqrt,atan2
from scipy import ndimage
from time import time
from scipy.signal import convolve2d
from cv2 import resize
import filters
from ipysheet import sheet, from_array, to_array
from ipysheet.utils import extract_data
import csv


def get_image(name,size):
    if name=='cameraman':
        im_in = rgb2gray(plt.imread('cameraman.bmp'))
        im_out = resize(im_in,dsize=(size,size))
    
    if name=='thankyou':
        im_in = rgb2gray(plt.imread('thankyou.jpg'))
        im_out = resize(im_in,dsize=(size,size))
    return im_out


def show_these(allplots,fsize=12):
    f = plt.figure(figsize=(fsize,fsize))
    n = len(allplots)
    k = 1
    num_cols = 2
    num_rows = n // 2 + 1 
    plt.subplots_adjust(wspace=1/n,hspace=1/n)
    for im in allplots:
        subplot_index = num_rows*100+num_cols*10+k
        ax = f.add_subplot(subplot_index)
        ax.imshow(im)
        k += 1
        
    
## Part 1
def plotwave(Amplitude,Frequency,Phase=0):
    times = np.linspace(0,5,1000)
    f = Amplitude * np.cos(2*pi*Frequency*times - Phase)
    plt.plot(times,f)
    plt.xlabel('Time, t [s]')
    plt.ylabel('Function, $f(t)$')
    plt.ylim([-5,5])
    return 
def play_with_wave():
    return interact(plotwave,Amplitude=IntSlider(description="Amplitude",value=2,min=-4,max=4),Frequency=IntSlider(description="Freq (Hz)",value=4,min=1,max=8),Phase=FloatSlider(description="Phase (rad)",value=0.0,min=-2*pi,max=2*pi,step=0.5),continuous_update=False)

# Part 2
def plotbox(width):
    N = 1000
    index = int(N/2)
    halfbox = int(width/2)
    signal = np.zeros(N)
    signal[index-halfbox:index+halfbox+1] = 1
    xaxis = np.linspace(-N/2,N/2,N)
    plt.plot(xaxis,signal)
    plt.xlabel('x axis')
    plt.ylabel('$b(x)$')
    plt.ylim([-0.5,1.5])
    return
def play_with_box_wave():    
    return interact(plotbox,width=IntSlider(description='Width (pixels)',value=100,min=50,max=500,step=50),continuous_update=False)

def getbox(width,N=1000,index=500,high=1,low=0):
    ''' returns a box function of lenght N and box size a 
        Note: signal[mid-halfbox] = high
              signal[mid+halfbox] = low
    '''
    signal = np.zeros(N)+low
    halfbox = int(width/2)
    signal[index-halfbox:index+halfbox+1] = high
    return signal

def getstep(index,N=1000,high=1,low=0):
    ''' returns a step function of lenght 1000 and box size a 
        Note: signal[mid-halfbox] = high
              signal[mid+halfbox] = low
    '''
    signal = np.zeros(N) + low
    signal[int(N/2)+index:] = high
    return signal
def play_with_reconstruction(Input):
    return interact(reconstruct,InputSignal=fixed(Input),tot_freq=IntSlider(description="# waves",value=50,min=1,max=100,step=1),return_label=fixed(False))

def reconstruct(InputSignal,tot_freq,return_label=False):
    a = [] #for cosing terms
    b = [] #for sin terms
    Amp = []
    Ph = []
    N = len(InputSignal) #This is the 'period' of the input function. 
    for freq in range(tot_freq):
        cosintegral = 0
        sinintegral = 0
        for index in range(N):
            cosintegral += InputSignal[index] * cos(index*2*np.pi*freq/N)
            sinintegral += InputSignal[index] * sin(index*2*np.pi*freq/N)
        a.append(2/N*cosintegral)
        b.append(2/N*sinintegral)
        Amp.append(sqrt(a[freq]**2+b[freq]**2))
        Ph.append(atan2(b[freq],a[freq]))

    recon = np.zeros(N)
    xaxis = np.linspace(-N/2,N/2,N)
    for freq in range(tot_freq):
        amp = Amp[freq]
        phase = Ph[freq]
        recon += amp * np.cos(2*np.pi*freq*xaxis - phase) 
    recon += -Amp[0]/2
    amplitudes = np.array(Amp)
    phases = np.array(Ph)
    if return_label:
        return recon,Amp,Ph
    else:
        plt.plot(xaxis,InputSignal,xaxis,recon)
        plt.xlabel('X axis (pixels)')
        plt.ylabel('Amplitude')
        plt.title('Reconstructed using sinusoidal waves')
def plot_random_lines(p=50,N=1000):
    random_input = np.random.rand(p)
    f = interp1d(range(p),random_input,kind='cubic')
    xnew = np.linspace(0,p-1,N)
    newinput = f(xnew)
    plt.plot(newinput)
    return newinput

def getwave(amp,freq,phase,length=1000):
    xaxis = np.linspace(-length/2,length/2,length)
    wave = np.zeros(length)+0.5
    wave += amp * np.cos(2*np.pi*freq*xaxis - phase)
    
    return wave

from mpl_toolkits.mplot3d import Axes3D

def check_first_few_waves(Input):
    return interact_manual(interactive_reconstruction,InputSignal=fixed(Input),F=IntSlider(description="# waves",value=3,min=1,max=15,step=1))

def interactive_reconstruction(InputSignal,F):
    F=F+1
    out = reconstruct(InputSignal,F,True)
    reconstructed = out[0]
    amplitudes = out[1]
    phases = out[2]
    frequencies = np.arange(F)
    length = len(reconstructed)
    xaxis = np.linspace(-length/2,length/2,length)
    ones = np.ones(1000,dtype=int)
    fig = plt.figure()
    ax = Axes3D(fig)

    for index in range(F+1):
        if index == 0:
            yaxis = ones*index
            ax.plot(xaxis,yaxis,InputSignal)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')
        elif index == 1:
            yaxis = ones*index
            ax.plot(xaxis,yaxis,reconstructed)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')            
        else:
            wave = getwave(amplitudes[index-1],frequencies[index-1],phases[index-1])
            yaxis = ones*index
            ax.plot(xaxis,yaxis,wave)
            ax.set_xlabel('X axis')
            ax.set_ylabel('Spectral Order')
            ax.set_zlabel('Amplitude')


def get_random_line(p,N=1000):
    random_input = np.random.rand(p)
    f = interp1d(range(p),random_input)
    xnew = np.linspace(0,p-1,N)
    newinput = f(xnew)
    return newinput

# Part 3

def addwaves(waves,const=0,tot_time=1,numpoints=10000):
    times = np.linspace(0,tot_time,numpoints)
    dt = times[1]-times[0]
    signal = np.zeros(numpoints)+const
    for wave in waves:
        amp = wave[0]
        freq = wave[1]
        phase = wave[2]
        w = amp*np.cos(2*np.pi*freq*times-phase)
        signal += w
    
    return signal,times

def plot_fft_1d_signal(signal,flimit=20):
    dft1 = np.fft.fft(signal)
    width = signal.size
    freq1 = np.fft.fftfreq(width,1/width)
    plt.bar(freq1,abs(dft1)/(width))
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    plt.xlim([-flimit,flimit])

def plot_1Dwave_and_fft(waves,const=5,tot_time=1,numpoints=1000,flimit=100,plotwidth=20,plotheight=5):
    signal,timesignal = addwaves(waves,const,tot_time,numpoints)
    f = plt.figure(figsize=(plotwidth,plotheight))
    ax1 = f.add_subplot(121)
    ax1.plot(timesignal,signal)
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    
    dft = np.fft.fft(signal)
    dt = tot_time/numpoints
    freqs = np.fft.fftfreq(numpoints,dt)
    plt.subplots_adjust(wspace=0.5)
    ax2 = f.add_subplot(122)
    ax2.bar(freqs,abs(dft)/numpoints)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    plt.xlim([-flimit,flimit])
    
def play_with_three_sine_waves():
    grid = widgets.GridspecLayout(4,2)
    amp1slider = IntSlider(description="Amp 1",value=1,min=-5,max=5)
    freq1slider = IntSlider(description="Freq 1",value=10,min=1,max=99)
    amp2slider = IntSlider(description="Amp 2",value=1,min=-5,max=5)
    freq2slider = IntSlider(description="Freq 2",value=10,min=1,max=99)
    amp3slider = IntSlider(description="Amp 3",value=1,min=-5,max=5)
    freq3slider = IntSlider(description="Freq 3",value=10,min=1,max=99)
    const_slider = IntSlider(description="AverageVal",value=5,min=-5,max=10)
    grid[0,0] = amp1slider
    grid[0,1] = freq1slider
    grid[1,0] = amp2slider
    grid[1,1] = freq2slider
    grid[2,0] = amp3slider
    grid[2,1] = freq3slider
    grid[3,0] = const_slider
      
    return interact_manual(plot_three_waves,a1=amp1slider,f1=freq1slider,a2=amp2slider,f2=freq2slider,a3=amp3slider,f3=freq3slider,const=const_slider,tot_time=fixed(1),numpoints=fixed(1000))

def DFT(x,shifted=False):
    """
    Compute the discrete Fourier Transform of the 1D array x
    :param x: (array)
    """
    N = x.size
    n = np.arange(N)
    k = n.reshape((N, 1))
    e = np.exp(-2j * np.pi * k * n / N)
    dft = np.dot(e, x)
    if shifted:
        shifted = np.copy(dft)
        shifted[0:int(N/2)] = dft[int(N/2):]
        shifted[int(N/2):] = dft[0:int(N/2)]
        return shifted
    else:
        return dft
    
def plot_three_waves(a1,f1,a2,f2,a3,f3,const=5,tot_time=1,numpoints=1000):
    waves = [(a1,f1,0),(a2,f2,0),(a3,f3,0)]
    plot_1Dwave_and_fft(waves,const,tot_time,numpoints)
    
def get_sample_data():
    csv.

# Part 4
    
def get_pixel_values(Image):
    return interact_manual(display_df,Image=fixed(Image),i=IntSlider(description="Shift Y",value=50,max=240,min=5),j=IntSlider(description="Shift X",value=50,max=240,min=5),box=fixed(8),continuous_update=False)

def display_df(Image,i,j,box):
    df = pd.DataFrame(Image)
    df1 = df.iloc[int(i-box/2):int(i+box/2),int(j-box/2):int(j+box/2)]
   # df1_styled = df1.style.background_gradient(cmap='viridis')
    rect = patches.Rectangle((j-box/2,i-box/2),box,box,fill=False)
    f = plt.figure(figsize=(8,8))
    ax2 = f.add_subplot(122)
    ax2.imshow(Image)
    ax2.add_patch(rect)
    display(df1)
    
# Part 5
def plot_2D_wave():
    return interact(plot2Dsine,fx=IntSlider(description="Freq in x",value=2,min=1,max=50),fy=IntSlider(description="Freq in y",value=2,min=0,max=50),phase=FloatSlider(description="Phase",value=0,min=-3,max=3,step=0.5),amplitude=IntSlider(description="Amplitude",value=2,min=-10,max=10),width=fixed(50),height=fixed(50),continuous_update=False)
    
def fft(f):
    if f.ndim == 2:
        return np.fft.fftshift(np.fft.fft2(f))
    if f.ndim == 1:
        return np.fft.fftshift(np.fft.fft(f))

def ifft(f):
    if f.ndim == 2:
        return np.fft.ifftshift(np.fft.ifft2(f))
    if f.ndim == 1:
        return np.fft.ifftshift(np.fft.ifft(f))
    
def reconstruct_image(InputImage,N,return_label=False):
    M = N
    (a,b) = InputImage.shape
    f_recon = np.zeros((a,b))
    C = np.zeros((a,b))
    nvc = np.matrix(np.linspace(1,N,N))
    mvc = np.matrix(np.linspace(1,M,M))
    xvc = np.matrix(np.linspace(1,a,a))
    yvc = np.matrix(np.linspace(1,b,a))
    print(nvc.shape)
    print(mvc.shape)
    print(xvc.shape)
    print(yvc.shape)
        #% C [NxM] = sin() [Nx1 X 1xA] * F [AxB] * sin() [Bx1 X 1xM]
    temp1 = nvc.transpose() * xvc
    temp2 = yvc.transpose() * mvc
    C = np.sin ( (np.pi / a ) * temp1 ) * InputImage * np.sin ( (np.pi/b) * temp2 )
    
     #       % F [AxB] = sin()[Ax1 X 1xN] * C[NxM] * sin()[Mx1 X 1xB]
    
    f_recon =  ( np.sin ( (np.pi / a ) * xvc.transpose() * nvc) ) * C * np.sin ( (np.pi/b) * mvc.transpose() * yvc ) 
            
    f_recon = 255 * (f_recon-f_recon.min()) / (f_recon.max() - f_recon.min())
    
    return f_recon
       
def get_fourierwave(fft,i,j,return_label=False):
   # From i,j get correct io,jo
   (width,height) = fft.shape
   midx = int(width/2)
   midy = int(height/2)
   io,jo = midy-i,midx+j
   z = fft[io,jo]
   amp = abs(z)
   phase = cmath.phase(z)
   #print(amp)
   #print(phase)
   xfreqs = np.fft.fftfreq(width,1/width)
   yfreqs = np.fft.fftfreq(height,1/height)
   maxamp = abs(fft[128-1,128+1])
   minamp = -maxamp
   #print(maxamp)
   #print(minamp)
   xv,yv = np.meshgrid(np.arange(width),np.arange(height))
   fourierwave = amp * np.sin(2*np.pi*j*xv/width+2*np.pi*i*yv/height - phase)
   #print(fourierwave.max())
   #print(fourierwave.min())
   (width,height) = fft.shape
   xfreqs = np.fft.fftfreq(width,1/width)
   yfreqs = np.fft.fftfreq(height,1/height)
   xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
   
   f = plt.figure(figsize=(8,8))
   ax1=f.add_subplot(121)
   ax1.imshow(fourierwave,vmin=minamp,vmax=maxamp)
   ax2=f.add_subplot(122)
   ft_magnitude = abs(fft)/(abs(fft)).size
   ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
   ax2.plot([j], [i],'r*')
   #return fourierwave

'''
def colorize(z):
    n,m = z.shape
    c = np.zeros((n,m,3))
    c[np.isinf(z)] = (1.0, 1.0, 1.0)
    c[np.isnan(z)] = (0.5, 0.5, 0.5)

    idx = ~(np.isinf(z) + np.isnan(z))
    A = (np.angle(z[idx]) + np.pi) / (2*np.pi)
    A = (A + 0.5) % 1.0
    B = 1.0 - 1.0/(1.0+abs(z[idx])**0.3)
    c[idx] = [hls_to_rgb(a, b, 1) for a,b in zip(A,B)]
    return c
'''

def colorize(z):
    r = np.abs(z)
    arg = np.angle(z) 
    h = (arg + np.pi)  / (2 * np.pi) + 0.5
    s = 1.0 - 1.0/(1.0 + r**0.3)
    v = 0.8

    c = np.vectorize(hsv_to_rgb) (h,s,v) # --> tuple
    c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
    c = c.swapaxes(0,2) 
    c = c.swapaxes(0,1)
    
    return c
from mpl_toolkits.axes_grid1 import make_axes_locatable

def plot2Dsine(fx,fy,phase,amplitude,width=50,height=50):
    x = np.arange(0,width)
    y = np.arange(0,height)
    xv,yv = np.meshgrid(x,y)
    signal = np.zeros((width,height))
    signal = amplitude*np.sin(2*np.pi*fx*xv/width+2*np.pi*fy*yv/height+phase)
    #for i in range(height):
    #    for j in range(width):
    #        signal[i,j] = 1*math.sin(2*math.pi*fx*j/width+2*math.pi*fy*i/height)
            #signal[i,j] = 1*math.sin(2*math.pi*fx*j/width)+1*math.sin(2*math.pi*fy*i/height)

  
    dft2 = fft(signal)/signal.size
    
    xfreqs = np.fft.fftfreq(width,1/width)
    xmin,xmax = xfreqs.min(),xfreqs.max()
    yfreqs = np.fft.fftfreq(height,1/height)
    ymin,ymax = yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=(15,15))
    ax1 = f.add_subplot(121)
    ax1.imshow(signal,vmin=-10,vmax=10)
    ax2 = f.add_subplot(122)
    plt.subplots_adjust(wspace=0.5)
    f1 = ax2.imshow(colorize(dft2),vmin=-np.pi,vmax=np.pi,interpolation='none',extent=[xmin,xmax,ymin,ymax])
    plt.xlabel('Freq in x (/pix)')
    plt.ylabel('Freq in y (/pix)')
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.5)
    f.colorbar(f1,cax=cax)


def see_fourier_waves(ft):
    return interact(get_fourierwave,fft=fixed(ft),i=IntSlider(description="Freq in Y",value=2,min=-120,max=120),j=IntSlider(description="Freq in X",value=2,min=-120,max=120),return_label=fixed(False),continuous_update=False)

def add_selected_waves(ft):
    return interact_manual(reconstruct_using_fft2d,fft=fixed(ft),ic=IntSlider(description="Shift Y",value=0,min=-120,max=120),jc=IntSlider(description="Shift X",value=0,min=-120,max=120),boxwidth=IntSlider(description="Box Width",value=60,min=10,max=120,step=10),boxheight=IntSlider(description="Box Height",value=60,min=10,max=120,step=10))

def add_using_IFFT(ft):
    return interact_manual(reconstruct_using_ifft2d,fft=fixed(ft),ic=IntSlider(description="Shift Y",value=0,min=-120,max=120),jc=IntSlider(description="Shift X",value=0,min=-120,max=120),boxwidth=IntSlider(description="Box Width",value=60,min=10,max=120,step=10),boxheight=IntSlider(description="Box Height",value=60,min=10,max=120,step=10))

def get_fourier_coefficients(fft,ic,jc,boxwidth,boxheight):
    (width,height) = fft.shape
    istart,jstart = int(width/2-ic-boxheight/2),int(width/2+jc-boxwidth/2)
    iend,jend=istart+boxheight,jstart+boxwidth
    fft_crop = fft[istart:iend,jstart:jend]
    amps = abs(fft_crop)/fft_crop.size
    phases = np.angle(fft_crop)
    xfreqs = np.arange(int(jc-boxwidth/2),int(jc+boxwidth/2))
    yfreqs = np.arange(int(ic-boxheight/2),int(ic+boxheight/2))
    xvf,yvf = np.meshgrid(xfreqs,yfreqs)
    waves = np.dstack((amps,phases,xvf,yvf))
    recaray=np.rec.array(waves,dtype=[('amp',float),('phase',float),('fx',float),('fy',float)])
    waverecord = np.ndarray.flatten(recaray)
    return waverecord

def add_fourierwaves(waves,shape):
    signal = np.zeros(shape)
    width = shape[0]
    height = shape[1]
    x = np.arange(width)
    y = np.arange(height)
    xv,yv = np.meshgrid(x,y)
    for wave in waves:
        amp = wave.amp
        phase = wave.phase
        xfreq = wave.fx
        yfreq = wave.fy
        signal += amp*np.cos(2*np.pi*xfreq*xv/width+2*np.pi*yfreq*yv/height+phase)
    #signal = np.fft.fftshift(signal)
    return signal

def reconstruct_using_fft2d(fft,ic,jc,boxwidth,boxheight):
    tic = time()
    waves = get_fourier_coefficients(fft,ic,jc,boxwidth,boxheight)
    reconstructed_image = add_fourierwaves(waves,fft.shape)
    (width,height) = fft.shape
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
   
    f = plt.figure(figsize=(8,8))
    ax1=f.add_subplot(121)
    ax1.imshow(reconstructed_image)
    ax2=f.add_subplot(122)
    ft_magnitude = abs(fft)/(abs(fft)).size
    rect = patches.Rectangle((jc-boxwidth/2,ic-boxheight/2),boxwidth,boxheight,fill=False)
    ax2.add_patch(rect)
    ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    ax2.plot([jc], [ic],'r*')
    print("Time take to reconstruct: "+str(time()-tic)+" s")

def get_amp_and_phase_at_freq(fft,freq):
    mask=create_concentric_mask(freq,1,fft.shape)
    crop_fft = fft * mask
    amps = abs(crop_fft)
    phases = np.angle(crop_fft)
    
    length = np.count_nonzero(amps)
    amp = amps.sum()/length
    phase = phases.sum()/length
    
    return amp,phase,freq
    
def reconstruct_using_ifft2d(fft,ic,jc,boxwidth,boxheight):
    tic = time()
    mask = np.zeros(fft.shape)    
    (width,height) = fft.shape
    istart,iend,jstart,jend = int(height/2-ic-boxheight/2),int(height/2-ic+boxheight/2),int(width/2+jc-boxwidth/2),int(width/2+jc+boxwidth/2)
    mask[istart:iend,jstart:jend] = 1
    newfft = fft*mask
    newifft = np.fft.ifftshift(ifft(newfft))
    reconstruct = abs(newifft)/(abs(newifft)).max()
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=(12,12))
    plt.subplots_adjust(wspace=0.5)
    ax1=f.add_subplot(131)
    ax1.imshow(reconstruct)
    ax2=f.add_subplot(132)
    ft_magnitude = abs(fft)/fft.size
    rect = patches.Rectangle((jc-boxwidth/2,ic-boxheight/2),boxwidth,boxheight,fill=False)
    ax2.add_patch(rect)
    ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    ax2.plot([jc], [ic],'r*')
    ax3=f.add_subplot(133)
    #ax3.imshow(mask,extent=[xmin,xmax,ymin,ymax])
    ft_magnitude = abs(newfft)/newfft.size
    ax3.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
    
    print("Time take to reconstruct: "+str(time()-tic)+" s")

def plot_image_and_its_ft(Image,figsize=(8,8),widthspace=1,withPhase=False):
    (width,height) = Image.shape
    ft = fft(Image)/Image.size
    ft_magnitude = abs(ft)
    ft_phase = np.angle(ft)
    xfreqs = np.fft.fftfreq(width,1/width)
    yfreqs = np.fft.fftfreq(height,1/height)
    xmin,xmax,ymin,ymax = xfreqs.min(),xfreqs.max(),yfreqs.min(),yfreqs.max()
    
    f = plt.figure(figsize=figsize)
    ax1=f.add_subplot(131)
    ax1.imshow(Image)
    ax2=f.add_subplot(132)
    plt.subplots_adjust(wspace=widthspace)
    if withPhase:
        ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')
        ax3=f.add_subplot(133)
        ax3.imshow(ft_phase,interpolation='none',extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')
        
    else:
        ax2.imshow(np.log(ft_magnitude),extent=[xmin,xmax,ymin,ymax])
        plt.xlabel('Freq in x (/pix)')
        plt.ylabel('Freq in y (/pix)')

def shift_image(Image,shiftX,shiftY):
    Image = np.roll(Image,shiftX,axis=1)
    Image = np.roll(Image,shiftY,axis=0)
    plot_image_and_its_ft(Image,(15,15),widthspace=0.5,withPhase=True)

def rotate_image(Image,angle):
    rotated = ndimage.rotate(Image,angle,reshape=False)
    plot_image_and_its_ft(rotated,(15,15),widthspace=0.5,withPhase=True)
    
def translation_property(I):
    return interact(shift_image,Image=fixed(I),shiftX=IntSlider(value=0,min=-128,max=128),shiftY=IntSlider(value=0,min=-128,max=128))
   
def rotation_property(I):
    return interact(rotate_image,Image=fixed(I),angle=IntSlider(value=0,min=-180,max=180),continuous_update=False)


# part 6

def create_circular_mask(radius,shape):
    mask = np.zeros(shape)
    xc,yc = int(shape[0]/2),int(shape[1]/2)
    halfwidth = shape[0] // 2
    y,x = np.ogrid[-halfwidth: halfwidth, -halfwidth: halfwidth]
    mask = (x**2+y**2 <= radius**2).astype(np.int_).astype(np.int8)
    
    return mask

def create_concentric_mask(out_radius,thick,shape):
    mask_o = create_circular_mask(out_radius, shape)
    in_radius = out_radius - thick
    mask_i = create_circular_mask(in_radius, shape)
    mask = mask_o - mask_i
    
    return mask


def conv_with_mask_using_fft(i,mask):
    ft_i = fft(i)
    ft_mask = fft(mask)
    ft_conv = ft_i * ft_mask
    conv = ifft(ft_conv)
    conv = abs(conv)/conv.size
    return conv

def conv_with_mask_using_conv2d(i,mask):
    conv = convolve2d(i, mask,mode='same')
    return conv

def plot_convoluted_image(Image,radius,thick,convType='FFT'):
    #radius = IntSlider(value=100,min=20,max=128)
    #thick = IntSlider(value=10,min=1,max=20)
    tic = time()
    mask = create_concentric_mask(radius, thick, Image.shape)
    if convType == 'FFT':
        conv = conv_with_mask_using_fft(Image, mask)
    elif convType == 'Convolution':
        conv = conv_with_mask_using_conv2d(Image,mask)
    
    f = plt.figure(figsize=(12,12))
    plt.subplots_adjust(wspace=0.5)
    ax1=f.add_subplot(121)
    ax1.imshow(conv)
    ax2=f.add_subplot(122)
    ax2.imshow(mask)
    print('Time taken to convolve: '+str(time()-tic)+" s")
    
def play_with_convolution(Image):
    radius = IntSlider(value=100,min=1,max=128)
    thick = IntSlider(value=10,min=1,max=128)
    convType = widgets.Combobox(options=["FFT","Convolution"],value="FFT")
    
    return interact_manual(plot_convoluted_image,Image=fixed(Image), radius=radius, thick=thick,convType=[('FFT','FFT'),('Convolution','Convolution')])

def get_kernel_from_type(kerneltype,size):
    
    if kerneltype=='sharpen':
        kernel = filters.sharpen(size)
    elif kerneltype=='box':
        kernel = filters.box_blur(size)
    elif kerneltype=='gaussian':
        kernel = filters.gaussian_blur(size)
    
    return kernel
    
def plot_image_kernel_conv(Image,kerneltype,size,inputkernel=None):
    if kerneltype=='sobel':
        sobelx,sobely = filters.sobel(size,'xy')
        output_dx = convolve2d(Image, sobelx,mode='same')
        output_dy = convolve2d(Image, sobely,mode='same')
        output = np.sqrt(output_dx**2 + output_dy**2)
        df_dx = pd.DataFrame(sobelx)
        df_dy = pd.DataFrame(sobely)
        print('Sobel derivative in X: \n')
        display(df_dx)
        print('Sobel derivative in Y: \n')
        display(df_dy)
        print('Results cw from top-left: input image, sobel x, output, sobel y')
        show_these((Image,output_dx,output_dy,output))
    
    elif kerneltype=='custom':
        kernel = inputkernel
    else:
        kernel = get_kernel_from_type(kerneltype,size)
  
    if kerneltype != 'sobel':
        output = convolve2d(Image, kernel,mode='same')
        kernel_df = pd.DataFrame(kernel)
        print("The kernel used is: \n")
        display(kernel_df)
        print("Input image and after convolution")
        show_these((Image,output))
    
def play_with_image_kernels(Image):
    dict_ktype = [('Sobel','sobel'),('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(plot_image_kernel_conv,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2),inputkernel=fixed(None))

def extract_kernel_and_convolve(Image,isheet):
    kernel = to_array(isheet)
    plot_image_kernel_conv(Image,'custom',isheet.rows,kernel)

def display_button(sheet1):
    button = widgets.Button(description="Run kernel")
    out = widgets.Output()
    def on_button_clicked(_):
          # "linking function with output"
          with out:
              # what happens when we press the button
              data = extract_data(sheet1)
              output = np.zeros((size,size))
              for i in range(size):
                  for j in range(size):
                      output[i,j] = data[i][j]['value']
              print("Heya")
              return output
          print(output)
    # linking button and function together using a button's method   
    button.on_click(on_button_clicked)
    widgets.VBox([button,out])

def get_padded_kernel(kernel,shape):
    bigkernel = np.zeros(shape)
    kwidth,kheight = kernel.shape
    istart,jstart = shape[0]//2-kheight//2,shape[1]//2-kwidth//2
    bigkernel[istart:istart+kheight,jstart:jstart+kwidth] = kernel
    return bigkernel

def demonstrate_conv_theorem(Image,kerneltype,size,justshow=True):
    kernel = get_kernel_from_type(kerneltype,size)
    kernel = get_padded_kernel(kernel, Image.shape)
    convoluted = conv_with_mask_using_fft(Image, kernel)
    if justshow:
        show_these((Image,kernel,np.log(abs(fft(Image))/Image.size),np.log(abs(fft(kernel))/Image.size),convoluted))
    else:
        return convoluted

def convolution_theorem(Image):
    dict_ktype = [('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(demonstrate_conv_theorem,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2),justshow=fixed(True))
        
def get_timings_convolution_theorem(Image,kerneltype,size):
    tic=time()
    fft_convoluted = demonstrate_conv_theorem(Image,kerneltype,size,justshow=False)
    time_fft_convoluted = time()-tic
    
    toc = time()
    kernel = get_kernel_from_type(kerneltype,size)
    conv2d_convoluted = conv_with_mask_using_conv2d(Image,kernel)
    time_conv2d_convoluted = time()-toc
    
    print("Time take for FFT algorithm: "+str(time_fft_convoluted)+" s \n"+"Time take for Convolution algorithm: "+str(time_conv2d_convoluted)+" s \n")
    show_these((Image,fft_convoluted,Image,conv2d_convoluted))
         
def compare_fft_convolve(Image):
    dict_ktype = [('Sharpen','sharpen'),('Box blur','box'),('Gaussian blur','gaussian')]
    interact_manual(get_timings_convolution_theorem,Image=fixed(Image),kerneltype=dict_ktype,size=IntSlider(value=3,min=3,max=51,step=2))

def finale():
    Image = get_image('thankyou',256)
    plt.imshow(Image)
    print(":)")
        
        
    
        