# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 13:39:21 2020

@author: abharadwaj1
"""
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as image
def sobel(size=3,ax='xy'):
    sobelx = cv2.getDerivKernels(1, 0, size)
    sobely = cv2.getDerivKernels(0, 1, size)
    
    if ax.lower()=='x': 
        return np.outer(sobelx[0],sobelx[1])
    elif ax.lower()=='y':
        return np.outer(sobely[0],sobely[1])
    elif ax.lower()=='xy':
        return np.outer(sobelx[0],sobelx[1]),np.outer(sobely[0],sobely[1])
    
def sharpen(size=3):
    output = np.ones((size,size)) * -1
    output[size//2,size//2] = size**2
    
    return output

def box_blur(size=3):
    output = np.ones((size,size)) * 1/size**2
    return output

def gaussian_blur(size=3):
    std = size/4
    xo,yo = size//2,size//2
    y,x = np.ogrid[0:size,0:size]
    xv,yv = np.meshgrid(x,y)
    gauss_output = (np.exp(-1*( ((xv-xo)**2 / (2*std**2)) + ( (yv-yo)**2 / (2*std**2) ))))*1/(2*np.pi*std**2)
    return gauss_output


    
    
    
