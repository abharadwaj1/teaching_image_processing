# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 13:39:21 2020

@author: abharadwaj1
"""
import cv2
import numpy as np

def sobel(size=3,ax='xy'):
    sobelx = cv2.getDerivKernels(1, 0, size)
    sobely = cv2.getDerivKernels(0, 1, size)
    
    if ax.lower()=='x': 
        return sobelx
    elif ax.lower()=='y':
        return sobely
    elif ax.lower()=='xy':
        return sobelx,sobely
    
def sharpen(size=3):
    output = np.ones((size,size)) * -1
    output[size//2,size//2] = size**2
    
    return output

def box_blur(size=3):
    output = np.ones((size,size)) * 1/size**2
    return output

def gaussian_blur(size=3,std=1):
    y,x = np.ogrid[-size//2:size//2+1,-size//2:size//2+1]
    xv,yv = np.meshgrid(x,y)
    gauss_output = np.exp(-1*( (x**2 / (2*std**2)) + ( y**2**2 / (2*std**2) ))) 
    return gauss_output

    
    
    
